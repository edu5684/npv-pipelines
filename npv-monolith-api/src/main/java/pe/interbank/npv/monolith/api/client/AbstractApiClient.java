package pe.interbank.npv.monolith.api.client;

import java.net.URI;
import java.net.URISyntaxException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import pe.interbank.rivendell.commons.api.client.ApiMethod;

public abstract class AbstractApiClient<T extends ApiMethod> {

	private Logger LOGGER = LoggerFactory.getLogger(getClass());

	private String host;
	private RestTemplate restTemplate;

	public static final String X_CORRELATION_ID_HEADER = "X-Correlation-Id";

	protected AbstractApiClient(String host) {
		this.restTemplate = new RestTemplate();
		this.host = host;
	}

	/**
	 * Calls the provided endpoint for the configured host and provided Api Method
	 * @param method
	 * @param requestObject
	 * @param responseType
	 * @param headers 
	 * @return
	 */
	public <C> ResponseEntity<C> call(ApiMethod method, Object requestObject, Class<C> responseType, HttpHeaders headers) {
		ResponseEntity<C> response = null;
		try {
			RequestEntity<Object> requestEntity = new RequestEntity<Object>(requestObject,headers, method.getHttpMethod(), getURI(method));
			response = this.restTemplate.exchange(requestEntity , responseType);
		} catch (RestClientException e) {
			LOGGER.error("Error posting to endpoint {} for host {}", method.getEndpoint(), this.host);
			LOGGER.error(e.getMessage());
		} catch (URISyntaxException e) {
			LOGGER.error("Error creating URI for host: {} and endpoint: {}", this.host, method.getEndpoint());
			LOGGER.error(e.getMessage());
		}
	
		return response;
		
	}

	/**
	 * Generates the URI for the request
	 * @param method
	 * @return
	 * @throws URISyntaxException 
	 */
	private URI getURI(ApiMethod method) throws URISyntaxException {
		return new URI(this.host + method.getEndpoint());
	}
}
