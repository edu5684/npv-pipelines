FROM openjdk:8

ARG artifact_id 
ARG artifact_version

ENV artifact ${artifact_id}-${artifact_version}.jar

# Create app directory
RUN mkdir -p /usr/src/app/${artifact_id}

#VOLUME /data

# Install app dependencies
COPY npv-monolith-app/target/${artifact} /usr/src/app/${artifact_id}/${artifact} 
WORKDIR /usr/src/app/${artifact_id}

EXPOSE 8080

CMD ["sh", "-c", "java -jar ${artifact}"]