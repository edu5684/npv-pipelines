package pe.interbank.npv.monolith.controller;

import java.net.URI;
import java.util.Random;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import io.swagger.annotations.ApiOperation;
import pe.interbank.npv.monolith.service.AccountsService;
import pe.interbank.rivendell.commons.api.controller.BaseController;
import pe.interbank.rivendell.job.domain.Correlation;
import pe.interbank.theshire.accounts.domain.FinancialInstitution;
import pe.interbank.theshire.accounts.dto.AccountDto;

@RestController
@RequestMapping("/accounts")
public class AccountsController extends BaseController{

	@Autowired
	private AccountsService accountsService;
	
	@ApiOperation(value = "Account Creation", nickname = "Account Creation")
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Correlation> create(
			@RequestHeader(name = X_APPLICATION_ID_HEADER, required = true) String applicationId,
			@Valid @RequestBody AccountDto dto
	){
			dto.getAccount().setFinancialInstitution(FinancialInstitution.INTERBANK);
			String correlationId = ((Long)(new Random().nextLong())).toString();
			
			dto.setCorrelation(new Correlation(applicationId,correlationId));
			
			HttpHeaders headers = new HttpHeaders();
			headers.set("X-Application-Id", applicationId);
			headers.set("Ocp-Apim-Subscription-Key","01a8b5bf2d194445abad1de23090fe1b");
			headers.set("X-Correlation-Id", correlationId);
			
			
			this.accountsService.save(dto,headers);
	
			Correlation dto2 = new Correlation(applicationId,correlationId);
			URI location = ServletUriComponentsBuilder.fromCurrentContextPath()
					.path("/correlations/applications/{applicationId}/correlations/{correlationId}")
					.buildAndExpand( applicationId, correlationId).toUri();


			return new ResponseEntity<Correlation>(dto2, HttpStatus.OK);
			
	}
}
