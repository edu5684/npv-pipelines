package pe.interbank.npv.monolith.dto;

import org.springframework.http.HttpMethod;

import pe.interbank.rivendell.commons.api.client.ApiMethod;


public enum AccountsMethod implements ApiMethod {
	CREATE_ACCOUNTS("/accounts", HttpMethod.POST);

	private String endpoint;
	private HttpMethod httpMethod;

	private AccountsMethod(String endpoint, HttpMethod httpMethod) {
		this.endpoint = endpoint;
		this.httpMethod = httpMethod;
	}

	@Override
	public String getEndpoint() {
		return this.endpoint;
	}

	@Override
	public HttpMethod getHttpMethod() {
		return this.httpMethod;
	}

}
