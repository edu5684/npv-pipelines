package pe.interbank.npv.monolith;

import java.util.TimeZone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import pe.interbank.npv.monolith.dto.AccountsClient;
import pe.interbank.npv.monolith.dto.CorrelationsClient;


/**
 * Hello world!
 *
 */
@SpringBootApplication(scanBasePackages = "pe.interbank")
public class NpvMicroservice 
{
	//@Value("${api.ext.host}")
	private String accountsHost="https://sixflags.azure-api.net/uat";
	
    public static void main( String[] args )
    {
        System.setProperty("spring.jackson.time-zone", TimeZone.getDefault().getID());
        SpringApplication.run(NpvMicroservice.class, args);
    }
    
	@Bean
	public AccountsClient accountsClient() {
		return new AccountsClient(accountsHost);
	}
	
	@Bean
	public CorrelationsClient correlationsClient() {
		return new CorrelationsClient(accountsHost);
	}
}
