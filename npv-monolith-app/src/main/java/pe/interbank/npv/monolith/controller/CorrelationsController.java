package pe.interbank.npv.monolith.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import pe.interbank.npv.monolith.service.CorrelationsService;
import pe.interbank.rivendell.commons.api.controller.BaseController;


@RestController
@RequestMapping("/correlations")
public class CorrelationsController extends BaseController{

	@Autowired
	private CorrelationsService correlationService;
	
	
	@ApiOperation(value = "Find Correlation by id and application", nickname = "Find Correlation by customer id")
	@RequestMapping(method = RequestMethod.GET, path = "/{correlationId}")
	public Object findById(@PathVariable(name = "correlationId") String correlationId,
			@RequestHeader(name = X_APPLICATION_ID_HEADER, required = true) String applicationId) {

		HttpHeaders headers = new HttpHeaders();
		headers.set("Ocp-Apim-Subscription-Key","01a8b5bf2d194445abad1de23090fe1b");
		headers.set("Content-Type", "application/json");
		Object dto=  this.correlationService.findById(headers,applicationId, correlationId);
		
		return new ResponseEntity<Object>(dto, HttpStatus.OK).getBody();
	}

}
