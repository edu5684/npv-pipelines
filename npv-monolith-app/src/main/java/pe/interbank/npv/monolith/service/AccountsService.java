package pe.interbank.npv.monolith.service;

import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;

import pe.interbank.theshire.accounts.dto.AccountDto;

public interface AccountsService {
	
	public ResponseEntity<AccountDto> save(AccountDto accountDto, HttpHeaders headers);
	
}
