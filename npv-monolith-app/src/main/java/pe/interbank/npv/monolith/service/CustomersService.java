/**
 * 
 */
package pe.interbank.npv.monolith.service;

import pe.interbank.theshire.customers.domain.Customer;
import pe.interbank.theshire.customers.domain.IdentityDocument;


public interface CustomersService {

	Customer findById(String customerId);
	
	Customer findByIdentityDocument(IdentityDocument identityDocument);
	
	
}
