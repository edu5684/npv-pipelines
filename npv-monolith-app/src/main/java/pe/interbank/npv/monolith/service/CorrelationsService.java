package pe.interbank.npv.monolith.service;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;



public interface CorrelationsService {

	public HttpEntity findById( HttpHeaders headers, String applicationId,String correlationId);
}
