package pe.interbank.npv.monolith.dto;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import pe.interbank.rivendell.job.domain.Data;
import pe.interbank.rivendell.job.domain.JobStatus;

@JsonInclude(Include.NON_EMPTY)
public class Job implements Serializable {
	private static final long serialVersionUID = 6364941305880812222L;

	@JsonProperty("id")
	private String correlationId;
	@JsonProperty("status")
	private JobStatus status;
	@JsonProperty("data")
	private Data data;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'hh:mm:ss.SSSXXX", timezone="America/Lima")
	private Date date;
	
	public String getCorrelationId() {
		return correlationId;
	}

	public void setCorrelationId(String correlationId) {
		this.correlationId = correlationId;
	}
	
	public JobStatus getStatus() {
		return status;
	}

	public void setStatus(JobStatus status) {
		this.status = status;
	}
	public Data getData() {
		return data;
	}

	public void setData(Data data) {
		this.data = data;
	}
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
}
