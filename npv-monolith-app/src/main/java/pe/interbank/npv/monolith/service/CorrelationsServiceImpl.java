package pe.interbank.npv.monolith.service;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import pe.interbank.npv.monolith.dto.CorrelationsClient;
import pe.interbank.npv.monolith.dto.CorrelationsMethod;
import pe.interbank.rivendell.job.domain.Job;


@Service
public class CorrelationsServiceImpl implements CorrelationsService {

	@Autowired
	private CorrelationsClient correlationClient;

	@Override
	public HttpEntity<Object> findById(HttpHeaders headers, String applicationId, String correlationId) {
		
		String endpoint="/applications/"+applicationId+"/correlations/"+correlationId;
		CorrelationsMethod method=new CorrelationsMethod(endpoint, HttpMethod.GET );
		Job job =new Job();
		 ResponseEntity<Object> response = this.correlationClient.call(
				 method,
					job, 
					Object.class,
					headers);
		 
		 return response;
		 
		
	}
}
