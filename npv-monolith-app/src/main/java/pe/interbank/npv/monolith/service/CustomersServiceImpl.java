/**
 * 
 */
package pe.interbank.npv.monolith.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.interbank.theshire.customers.domain.Customer;
import pe.interbank.theshire.customers.domain.IdentityDocument;
import pe.interbank.theshire.customers.repository.CustomersRepository;

@Service
public class CustomersServiceImpl  implements CustomersService {

	@Autowired
	private CustomersRepository customersRepository;

	@Override
	public Customer findByIdentityDocument(IdentityDocument identityDocument) {
		return this.customersRepository.findByIdentityDocument(identityDocument);
	}

	@Override
	public Customer findById(String customer) {
		return this.customersRepository.findById(customer);
	}


	


}
