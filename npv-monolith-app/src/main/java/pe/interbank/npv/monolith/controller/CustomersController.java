/**
 * 
 */
package pe.interbank.npv.monolith.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import pe.interbank.rivendell.commons.api.controller.BaseController;
import pe.interbank.rivendell.commons.api.exception.NotFoundException;
import pe.interbank.theshire.customers.converter.IdentityDocumentTypeConverter;
import pe.interbank.theshire.customers.domain.Customer;
import pe.interbank.theshire.customers.domain.IdentityDocument;
import pe.interbank.theshire.customers.domain.IdentityDocumentType;
import pe.interbank.theshire.customers.dto.CustomerRequestDto;
import pe.interbank.npv.monolith.service.CustomersService;


@RestController
@RequestMapping("/customers")
public class CustomersController extends BaseController {
	public static final String PARAM_IDENTITY_DOCUMENT_NUMBER = "identityDocumentNumber";

	public static final String PARAM_IDENTITY_DOCUMENT_TYPE = "identityDocumentType";

	public static final String PARAM_CUSTOMER_ID = "customerId";

	private static final Logger LOGGER = LoggerFactory.getLogger(CustomersController.class);

	@Autowired
	private CustomersService customersService;

	@ApiOperation(value = "Find Customer by document number and type", nickname = "Find Customer by document number and type")
	@RequestMapping(method = RequestMethod.GET)
	public HttpEntity<CustomerRequestDto> findByidentityDocument(
			@RequestParam(value = PARAM_IDENTITY_DOCUMENT_TYPE, required = true) IdentityDocumentType identityDocumentType,
			@RequestParam(value = PARAM_IDENTITY_DOCUMENT_NUMBER, required = true) String identityDocumentNumber) {
		LOGGER.debug("findByidentityDocument {} {}", identityDocumentType, identityDocumentNumber);
		IdentityDocument identityDocument = new IdentityDocument(identityDocumentType, identityDocumentNumber);

		try {
			Customer customer = this.customersService.findByIdentityDocument(identityDocument);
			CustomerRequestDto dto = new CustomerRequestDto(customer);
			return new ResponseEntity<CustomerRequestDto>(dto, HttpStatus.OK);
		} catch (NotFoundException e) {
			return ResponseEntity.notFound().build();
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}
	}

	@ApiOperation(value = "Find Customer by customer id", nickname = "Find Customer by customer id")
	@RequestMapping(method = RequestMethod.GET, path = "/{customerId}")
	public HttpEntity<CustomerRequestDto> findById(@PathVariable(name = "customerId") String customerId) {

		Customer customer = this.customersService.findById(customerId);
		CustomerRequestDto dto = new CustomerRequestDto(customer);

		return new ResponseEntity<CustomerRequestDto>(dto, HttpStatus.OK);
	}


	@InitBinder
	public void initBinder(final WebDataBinder webdataBinder) {
		webdataBinder.registerCustomEditor(IdentityDocumentType.class, new IdentityDocumentTypeConverter());
	}

}
