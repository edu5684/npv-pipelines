package pe.interbank.npv.monolith.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import pe.interbank.npv.monolith.dto.AccountsClient;
import pe.interbank.npv.monolith.dto.AccountsMethod;
import pe.interbank.theshire.accounts.dto.AccountDto;

@Service
public class AccountsServiceImpl  implements AccountsService {

	@Autowired
	private AccountsClient accountsClient;
	
	@Override
	public ResponseEntity<AccountDto> save(AccountDto dto, HttpHeaders headers) {
		
		 ResponseEntity<AccountDto> response = this.accountsClient.call(
					AccountsMethod.CREATE_ACCOUNTS, 
					dto, 
					AccountDto.class,
					headers);
		 return response;
	}
}
