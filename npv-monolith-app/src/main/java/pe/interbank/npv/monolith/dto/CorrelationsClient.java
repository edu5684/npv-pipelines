package pe.interbank.npv.monolith.dto;

import pe.interbank.npv.monolith.api.client.AbstractApiClient;

public class CorrelationsClient extends AbstractApiClient<CorrelationsMethod> {

		public CorrelationsClient(String host) {
			super(host);
		}
}
