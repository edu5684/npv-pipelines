package pe.interbank.npv.monolith.dto;

import org.springframework.http.HttpMethod;

import pe.interbank.rivendell.commons.api.client.ApiMethod;

public class CorrelationsMethod implements ApiMethod{
	
	private String endpoint;
	private HttpMethod httpMethod;

	public CorrelationsMethod(String endpoint, HttpMethod httpMethod) {
		this.endpoint = endpoint;
		this.httpMethod = httpMethod;
	}

	@Override
	public String getEndpoint() {
		return this.endpoint;
	}

	@Override
	public HttpMethod getHttpMethod() {
		return this.httpMethod;
	}
}
